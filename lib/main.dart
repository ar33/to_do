import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo/view/home.dart';
import 'package:todo/viewmodel/addTask/add_task_bloc.dart';
import 'package:todo/viewmodel/countTask/count_task_bloc.dart';
import 'package:todo/viewmodel/kategori/kategori_bloc.dart';
import 'package:todo/viewmodel/profile/profile_bloc.dart';
import 'package:todo/viewmodel/viewTask/view_task_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<KategoriBloc>(
          create: (context) => KategoriBloc(),
        ),
        BlocProvider<AddTaskBloc>(
          create: (context) => AddTaskBloc(),
        ),
        BlocProvider<ProfileBloc>(
          create: (context) => ProfileBloc(),
        ),
        BlocProvider<ViewTaskBloc>(
          create: (context) => ViewTaskBloc(),
        ),
        BlocProvider<CountTaskBloc>(
          create: (context) => CountTaskBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: home(),
      ),
    );
  }
}

