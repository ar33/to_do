part of 'count_task_bloc.dart';

@immutable
abstract class CountTaskEvent {}

class getCountTask extends CountTaskEvent {}

class clearCount extends CountTaskEvent {}
