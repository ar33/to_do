import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:todo/model/task.dart';
import 'package:todo/setting.dart';
import 'package:todo/view/addTask.dart';
import 'package:todo/view/profile.dart';
import 'package:todo/viewmodel/addTask/add_task_bloc.dart';
import 'package:todo/viewmodel/viewTask/view_task_bloc.dart';

enum AnswerDialog { ok, batal }

class detailHome extends StatelessWidget {
  detailHome(this.kategori, this.index);

  int index;
  list kategori;
  AddTaskBloc _addTask;
  ViewTaskBloc _taskBloc;

  @override
  Widget build(BuildContext context) {
    _addTask = BlocProvider.of<AddTaskBloc>(context);
    _taskBloc = BlocProvider.of<ViewTaskBloc>(context);
    return BlocBuilder<ViewTaskBloc, ViewTaskState>(
      builder: (context, state) => Scaffold(
        backgroundColor: primaryColor,
        appBar: AppBar(
          leading: IconButton(
              tooltip: "Back",
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              )),
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(""),
          actions: [
            IconButton(
              tooltip: "My Account",
              icon: Icon(
                Icons.people,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => profile(),
                    ));
              },
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
              // height: 130,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CircleAvatar(
                    child: Icon(
                      kategori.iconList,
                      color: kategori.iconColor,
                    ),
                    radius: 15,
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    kategori.title,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 24),
                  ),
                  Text(
                    "",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Text(
                      "Late",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListView.separated(
                      separatorBuilder: (context, index) => Container(
                        height: 10,
                      ),
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.lateTask.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            _answerDialog(
                                context: context,
                                dataTask: state.lateTask[index]);
                          },
                          child: Container(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Text(
                                        state.lateTask[index].namaTask
                                            .toString(),
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        state.lateTask[index].tanggal
                                            .toString(),
                                        style: TextStyle(color: Colors.red),
                                      )
                                    ],
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                  ),
                                ),
                                Icon(
                                  Icons.check_box_outline_blank,
                                  color: Colors.grey[300],
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Today",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListView.separated(
                      separatorBuilder: (context, index) => Container(
                        height: 10,
                      ),
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.todayTask.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            _answerDialog(
                                context: context,
                                dataTask: state.todayTask[index]);
                          },
                          child: Container(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Text(
                                        state.todayTask[index].namaTask,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(state.todayTask[index].tanggal)
                                    ],
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                  ),
                                ),
                                Icon(
                                  Icons.check_box_outline_blank,
                                  color: Colors.grey[300],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Done",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListView.separated(
                      separatorBuilder: (context, index) => Container(
                        height: 10,
                      ),
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.doneTask.length,
                      itemBuilder: (context, index) {
                        var inputFormat = DateFormat('yyyy-MM-dd HH:mm');
                        DateTime selectedDate =
                            inputFormat.parse(state.doneTask[index].udate);
                        final DateFormat format = DateFormat('yyyy-MM-dd');
                        String tanggalSelesai = format.format(selectedDate);
                        return Container(
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      state.doneTask[index].namaTask,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text("${state.doneTask[index].tanggal} - " +
                                        "$tanggalSelesai")
                                  ],
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                              ),
                              Icon(
                                Icons.check_box_outline_blank,
                                color: Colors.red[300],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => addTask(),
                )).then((value) {
              _taskBloc.add(getTaskByKategori(index));
              print("refresh");
            });
          },
          backgroundColor: primaryColor,
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Future<void> _answerDialog({BuildContext context, Task dataTask}) async {
    switch (await showDialog<AnswerDialog>(
        context: context,
        // barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            child: Container(
              constraints: BoxConstraints(
                minWidth: 10,
                minHeight: 10,
              ),
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    "Warning!",
                    style: TextStyle(color: Colors.red),
                  ),
                  Divider(),
                  Text("Are you sure to close this Task?"),
                  Divider(),
                  Container(
                    height: 20,
                    child: Row(
                      children: [
                        Expanded(
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("No"),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: RaisedButton(
                            onPressed: () {
                              _taskBloc.add(getTaskByKategori(index));
                              _addTask.add(updateTask(dataTask));
                              Navigator.pop(context);
                            },
                            child: Text("Yes"),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        })) {
      case AnswerDialog.ok:
        break;
      case AnswerDialog.batal:
        break;
    }
  }
}
