import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo/setting.dart';
import 'package:todo/view/detailHome.dart';
import 'package:todo/view/profile.dart';
import 'package:todo/viewmodel/countTask/count_task_bloc.dart';
import 'package:todo/viewmodel/kategori/kategori_bloc.dart';
import 'package:todo/viewmodel/viewTask/view_task_bloc.dart';

import 'addTask.dart';

class home extends StatelessWidget {
  KategoriBloc _kategoriBloc;
  ViewTaskBloc _taskBloc;
  CountTaskBloc _countTaskBloc;

  @override
  Widget build(BuildContext context) {
    _kategoriBloc = BlocProvider.of<KategoriBloc>(context);
    _taskBloc = BlocProvider.of<ViewTaskBloc>(context);
    _countTaskBloc = BlocProvider.of<CountTaskBloc>(context);
    return BlocBuilder<CountTaskBloc, CountTaskState>(
      builder: (context, state) {
        if (state is CountTaskInitial) {
          _countTaskBloc.add(getCountTask());
        }
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            backgroundColor: primaryColor,
            child: Icon(Icons.add),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => addTask(),
                  )).then((value) {
                print("refresh");
                _countTaskBloc.add(getCountTask());
              });
            },
          ),
          appBar: AppBar(
            leading: IconButton(
              tooltip: "My Account",
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => profile(),
                    )).then((value) {
                  _countTaskBloc.add(getCountTask());
                });
              },
              icon: Icon(
                Icons.people,
                color: Colors.black,
              ),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    "Lists",
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  )),
              Expanded(
                child: GridView.builder(
                  padding: EdgeInsets.all(10),
                  itemCount: 6,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    childAspectRatio: 100 / 100,
                  ),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            _taskBloc.add(getTaskByKategori(index));
                            return detailHome(dataList[index], index);
                          },
                        )).then((value) {
                          print("refresh");
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(color: Colors.grey[100]),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey[200],
                                  blurRadius: 5,
                                  offset: Offset(0, 5))
                            ]),
                        child: Column(
                          children: [
                            Icon(
                              dataList[index].iconList,
                              color: dataList[index].iconColor,
                              size: 40,
                            ),
                            Spacer(),
                            Text(
                              "${dataList[index].title}",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              (state.dataCount != null)
                                  ? "${state.dataCount[index].count} Task"
                                  : "0",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                            )
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
