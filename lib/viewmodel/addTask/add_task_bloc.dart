import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo/model/task.dart';
import 'package:todo/sqlite/sqliteHelper.dart';

part 'add_task_event.dart';

part 'add_task_state.dart';

class AddTaskBloc extends Bloc<AddTaskEvent, AddTaskState> {
  AddTaskBloc() : super(AddTaskInitial());
  DbHelper dbHelper = DbHelper();

  @override
  Stream<AddTaskState> mapEventToState(
    AddTaskEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is createTask) {
      int respon;
      final Future<Database> dbFuture = dbHelper.initDb();
      await dbFuture.then((database) async {
        respon = await dbHelper.insert_data_task(event.dataTask);
      });
      print("respon");
      print(respon);
      if (respon == 1) {
        yield successPost();
      } else {
        yield failedPost();
      }
    } else if (event is updateTask) {
      int respon;
      final Future<Database> dbFuture = dbHelper.initDb();
      await dbFuture.then((database) async {
        respon = await dbHelper.update_data_task(event.dataTask);
      });
      if (respon == 1) {
        yield successPost();
      } else {
        yield failedPost();
      }
    } else {
      yield AddTaskInitial();
    }
  }
}
