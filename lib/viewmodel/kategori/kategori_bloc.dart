import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo/model/kategori.dart';
import 'package:todo/sqlite/sqliteHelper.dart';
import 'package:todo/setting.dart';

part 'kategori_event.dart';

part 'kategori_state.dart';

class KategoriBloc extends Bloc<KategoriEvent, KategoriState> {
  KategoriBloc() : super(KategoriInitial());
  DbHelper dbHelper = DbHelper();

  @override
  Stream<KategoriState> mapEventToState(
    KategoriEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is getKategori) {
      List<Kategori> listKategori = List<Kategori>();
      final Future<Database> dbFuture = dbHelper.initDb();
      await dbFuture.then((database) async {
        Future<List<Kategori>> transaksiListFuture = dbHelper.getListKategori();
        await transaksiListFuture.then((transaksiList) {
          listKategori = transaksiList;
        });
      });
      dataKategori = listKategori;
      yield setKategoriState(listKategori);
    }
  }
}
