import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:todo/model/countTask.dart';
import 'package:todo/model/kategori.dart';
import 'package:todo/model/task.dart';

class DbHelper {
  static DbHelper _dbHelper;
  static Database _database;

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }

  int a = 0;

  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + '/todo.db';
    var todoDatabase = openDatabase(
      path,
      version: 1,
      onCreate: _createDb,
    );

    return todoDatabase;
  }

  void _createDb(Database db, int version) async {
    print("coba create");
    await db.execute('''
      CREATE TABLE task (
        id_task INTEGER PRIMARY KEY AUTOINCREMENT,
        nama_task varchar(24),
        id_kategori INTEGER,
        note varchar(24),
        tanggal Date,
        tdate DateTime,
        udate DateTime,
        status INTEGER
      )
    ''');
    await db.execute('''
      CREATE TABLE kategori (
        id_kategori INTEGER PRIMARY KEY AUTOINCREMENT,
        nama_kategori varchar(50),
        status INTEGER
      )
    ''');
    // await db.execute(
    //     'INSERT INTO migrasi_database VALUES(1, "1", "${DateTime.now()}")');
    await db.execute('INSERT INTO kategori VALUES(1, "Work", 1)');
    await db.execute('INSERT INTO kategori VALUES(2, "Music", 1)');
    await db.execute('INSERT INTO kategori VALUES(3, "Travel", 1)');
    await db.execute('INSERT INTO kategori VALUES(4, "Study", 1)');
    await db.execute('INSERT INTO kategori VALUES(5, "Home", 1)');

    print("Data Base Create ${DateTime.now()}");
    await db.close();
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  Future<List<Task>> getListTask({int id_kategori, int status}) async {
    var taskMapList =
        await select_data_task(id_kategori: id_kategori, status: status);
    int count = taskMapList.length;
    List<Task> listTask = List<Task>();
    for (int i = 0; i < count; i++) {
      listTask.add(Task.fromMap(taskMapList[i]));
    }
    return listTask;
  }

  Future<List<Map<String, dynamic>>> select_data_task(
      {int id_kategori, int status}) async {
    Database db = await this.database;
    DateTime selectedDate = DateTime.now();
    final DateFormat format = DateFormat('yyyy-MM-dd');
    String tanggal = format.format(selectedDate);
    print(tanggal);
    if (id_kategori != 0) {
      if (status == 0) {
        print("By ID KATEGORI");
        var mapList = await db.query('task',
            orderBy: 'tdate DESC',
            where: "id_kategori=? and tanggal<? and status=1",
            whereArgs: [id_kategori, tanggal]);
        return mapList;
      } else if (status == 1) {
        var mapList = await db.query('task',
            orderBy: 'tdate DESC',
            where: "id_kategori=? and tanggal>=? and status=?",
            whereArgs: [id_kategori, tanggal, status]);
        return mapList;
      } else {
        print("By ID KATEGORI");
        var mapList = await db.query('task',
            orderBy: 'tdate DESC',
            where: "id_kategori=? and status=?",
            whereArgs: [id_kategori, status]);
        return mapList;
      }
    } else {
      if (status == 0) {
        print("NULL");
        var mapList = await db.query('task',
            orderBy: 'tdate DESC',
            where: "tanggal<? and status =1",
            whereArgs: [tanggal]);
        return mapList;
      } else if (status == 1) {
        print("NULL");
        var mapList = await db.query('task',
            orderBy: 'tdate DESC',
            where: "status=? and tanggal>=?",
            whereArgs: [status, tanggal]);
        return mapList;
      } else {
        print("NULL");
        var mapList = await db.query('task',
            orderBy: 'tdate DESC', where: "status=?", whereArgs: [status]);
        return mapList;
      }
    }
  }

  Future<List<Kategori>> getListKategori() async {
    var kategoriMapList = await select_data_kategori();
    int count = kategoriMapList.length;
    List<Kategori> listKategori = List<Kategori>();
    for (int i = 0; i < count; i++) {
      listKategori.add(Kategori.fromMap(kategoriMapList[i]));
    }
    print(listKategori.length);
    return listKategori;
  }

  Future<List<Map<String, dynamic>>> select_data_kategori() async {
    Database db = await this.database;
    var mapList = await db.query('kategori', orderBy: 'id_kategori'
        // ,where: "status=?"
        // , whereArgs: [id_kategori]
        );
    return mapList;
  }

  Future<List<CountTask>> getCountTask() async {
    var countList = await select_data_count_task();
    int count = countList.length;
    List<CountTask> listKategori = List<CountTask>();
    for (int i = 0; i < count; i++) {
      listKategori.add(CountTask.fromMap(countList[i]));
    }
    return listKategori;
  }

  Future<List<Map<String, dynamic>>> select_data_count_task() async {
    Database db = await this.database;
    var data = await db.rawQuery(""
        "select k.id_kategori, count(t.id_kategori) as count "
        "from kategori as k "
        "left join task as t on t.id_kategori = k.id_kategori "
        "group by k.id_kategori "
        "order by k.id_kategori");
    return data;
  }

  Future<int> insert_data_task(Task object) async {
    print(object.tanggal);
    Task data = object;
    int respon;
    data.tdate = DateTime.now().toString();
    data.udate = DateTime.now().toString();
    Database db = await this.database;
    int count = await db.insert('task', data.toMap());
    if (count >= 1) {
      respon = 1;
    }
    return respon;
  }

  Future<int> update_data_task(Task object) async {
    Task data = object;
    data.status = 2;
    Database db = await this.database;
    int count = await db.update('task', data.toMap(),
        where: 'id_task=?', whereArgs: [data.idTask]);
    return count;
  }
}
