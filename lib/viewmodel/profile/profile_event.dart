part of 'profile_bloc.dart';

@immutable
abstract class ProfileEvent {}

class getProfile extends ProfileEvent {
  getProfile();
}

class clearProfile extends ProfileEvent {
  clearProfile();
}
