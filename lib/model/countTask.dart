class CountTask {
  int _id_kategori;
  int _count;

  CountTask(this._id_kategori, this._count);

  CountTask.fromMap(Map<String, dynamic> map) {
    this._id_kategori = map['id_kategori'];
    this._count = map['count'];
  }

  int get idKategori => _id_kategori;

  int get count => _count;

  set idKategori(int value) {
    _id_kategori = value;
  }

  set count(int value) {
    _count = value;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id_kategori'] = idKategori;
    map['count'] = count;
    return map;
  }
}
