class Task {
  String _nama_task, _note;
  int _id_task, _id_kategori, _status;
  String _tanggal, _tdate, _udate;

  Task(this._id_kategori, this._note, this._tanggal, this._nama_task,
      this._status);

  Task.fromMap(Map<String, dynamic> map) {
    this._id_task = map['id_task'];
    this._id_kategori = map['id_kategori'];
    this._nama_task = map['nama_task'];
    this._note = map['note'];
    this._tanggal = map['tanggal'].toString();
    this._tdate = map['tdate'].toString();
    this._udate = map['udate'].toString();
    this._status = map['status'];
  }

  int get idTask => _id_task;

  int get idKategori => _id_kategori;

  String get namaTask => _nama_task;

  String get note => _note;

  String get tanggal => _tanggal;

  String get tdate => _tdate;

  String get udate => _udate;

  int get status => _status;

  set idTask(int value) {
    _id_task = value;
  }

  set idKategori(int value) {
    _id_kategori = value;
  }

  set namaTask(String value) {
    _nama_task = value;
  }

  set note(String value) {
    _note = value;
  }

  set tanggal(String value) {
    _tanggal = value;
  }

  set tdate(String value) {
    _tdate = value;
  }

  set udate(String value) {
    _udate = value;
  }

  set status(int value) {
    _status = value;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id_task'] = this._id_task;
    map['nama_task'] = namaTask;
    map['id_kategori'] = idKategori;
    map['note'] = note;
    map['tanggal'] = tanggal;
    map['tdate'] = tdate;
    map['udate'] = udate;
    map['status'] = status;
    return map;
  }
}
