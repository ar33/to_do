import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo/model/task.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo/sqlite/sqliteHelper.dart';

part 'view_task_event.dart';

part 'view_task_state.dart';

class ViewTaskBloc extends Bloc<ViewTaskEvent, ViewTaskState> {
  ViewTaskBloc() : super(ViewTaskInitial());
  DbHelper dbHelper = DbHelper();

  @override
  Stream<ViewTaskState> mapEventToState(
    ViewTaskEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is getTaskByKategori) {
      List<Task> lateTask = List<Task>();
      List<Task> todayTask = List<Task>();
      List<Task> doneTask = List<Task>();
      final Future<Database> dbFuture = dbHelper.initDb();
      await dbFuture.then((database) async {
        Future<List<Task>> transaksiListFuture =
            dbHelper.getListTask(id_kategori: event.id, status: 0);
        await transaksiListFuture.then((transaksiList) {
          lateTask = transaksiList;
        });
      });
      await dbFuture.then((database) async {
        Future<List<Task>> transaksiListFuture =
            dbHelper.getListTask(id_kategori: event.id, status: 1);
        await transaksiListFuture.then((transaksiList) {
          todayTask = transaksiList;
        });
      });
      await dbFuture.then((database) async {
        Future<List<Task>> transaksiListFuture =
            dbHelper.getListTask(id_kategori: event.id, status: 2);
        await transaksiListFuture.then((transaksiList) {
          doneTask = transaksiList;
        });
      });
      yield setTaskState(
          doneTask: doneTask, lateTask: lateTask, todayTask: todayTask);
    } else if (event is clearTaskByKategori) {
      yield ViewTaskInitial();
    }
  }
}
