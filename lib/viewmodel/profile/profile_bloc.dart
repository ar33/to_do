import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo/model/user.dart';

part 'profile_event.dart';

part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileInitial());

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is getProfile) {
      User dataProfile;
      dataProfile = await User.getDataProfile();
      yield setProfileState(dataProfile);
    } else if (event is clearProfile) {
      yield ProfileInitial();
    }
  }
}
