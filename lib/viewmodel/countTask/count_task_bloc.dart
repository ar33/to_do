import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo/model/countTask.dart';
import 'package:todo/sqlite/sqliteHelper.dart';

part 'count_task_event.dart';

part 'count_task_state.dart';

class CountTaskBloc extends Bloc<CountTaskEvent, CountTaskState> {
  CountTaskBloc() : super(CountTaskInitial());
  DbHelper dbHelper = DbHelper();

  @override
  Stream<CountTaskState> mapEventToState(
    CountTaskEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is getCountTask) {
      int totalTask = 0;
      List<CountTask> listCount = List<CountTask>();
      final Future<Database> dbFuture = dbHelper.initDb();
      await dbFuture.then((database) async {
        Future<List<CountTask>> transaksiListFuture = dbHelper.getCountTask();
        await transaksiListFuture.then((data) {
          listCount = data;
        });
      });
      listCount.forEach((element) {
        totalTask += element.count;
      });
      listCount.insert(0, CountTask(0, totalTask));
      print(listCount.length);
      yield setCountTask(listCount);
    } else if (event is clearCount) {
      yield CountTaskInitial();
    }
  }
}
