part of 'count_task_bloc.dart';

@immutable
abstract class CountTaskState {
  List<CountTask> dataCount;
}

class CountTaskInitial extends CountTaskState {}

class setCountTask extends CountTaskState {
  List<CountTask> dataCount;

  setCountTask(this.dataCount);
}
