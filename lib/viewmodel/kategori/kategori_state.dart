part of 'kategori_bloc.dart';

@immutable
abstract class KategoriState {
  List<Kategori> listKategori = List<Kategori>();

  KategoriState();
}

class KategoriInitial extends KategoriState {}

class setKategoriState extends KategoriState {
  List<Kategori> listKategori = List<Kategori>();

  setKategoriState(this.listKategori);
}
