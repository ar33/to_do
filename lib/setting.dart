import 'package:flutter/material.dart';
import 'package:todo/model/kategori.dart';

import 'model/user.dart';

List<list> dataList = [
  list(
    title: "All",
    subTitle: "",
    iconList: Icons.filter_list_sharp,
    iconColor: Colors.blue,
  ),
  list(
      title: "Work",
      subTitle: "",
      iconList: Icons.work_outline,
      iconColor: Colors.orange),
  list(
      title: "Music",
      subTitle: "",
      iconList: Icons.music_note,
      iconColor: Colors.red),
  list(
      title: "Travel",
      subTitle: "",
      iconList: Icons.airplanemode_on_outlined,
      iconColor: Colors.green),
  list(
      title: "Study",
      subTitle: "",
      iconList: Icons.book,
      iconColor: Colors.grey),
  list(
      title: "Home",
      subTitle: "",
      iconList: Icons.home_filled,
      iconColor: Colors.redAccent),
];

User dataUser;
List<Kategori> dataKategori;
Color primaryColor = Color.fromRGBO(83, 131, 250, 1);

class list {
  IconData iconList;
  String title, subTitle;
  Color iconColor;

  list({this.title, this.subTitle, this.iconList, this.iconColor});
}
