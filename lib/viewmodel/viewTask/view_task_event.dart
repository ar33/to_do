part of 'view_task_bloc.dart';

@immutable
abstract class ViewTaskEvent {
  int id;
}

class getTaskByKategori extends ViewTaskEvent {
  int id;

  getTaskByKategori(this.id);
}

class clearTaskByKategori extends ViewTaskEvent {
  clearTaskByKategori();
}
