class Kategori {
  String _nama_kategori;
  int _id_kategori, _status;

  Kategori(this._id_kategori, this._nama_kategori, this._status);

  Kategori.fromMap(Map<String, dynamic> map) {
    this._id_kategori = map['id_kategori'];
    this._nama_kategori = map['nama_kategori'];
    this._status = map['status'];
  }

  int get idKategori => _id_kategori;

  String get namaKategori => _nama_kategori;

  int get status => _status;

  set idKategori(int value) {
    _id_kategori = value;
  }

  set namaKategori(String value) {
    _nama_kategori = value;
  }

  set status(int value) {
    _status = value;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();

    map['id_kategori'] = idKategori;
    map['nama_kategori'] = namaKategori;
    map['status'] = status;

    return map;
  }
}
