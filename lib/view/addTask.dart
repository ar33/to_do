import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:todo/model/kategori.dart';
import 'package:todo/model/task.dart';
import 'package:todo/setting.dart';
import 'package:todo/viewmodel/addTask/add_task_bloc.dart';
import 'package:todo/viewmodel/kategori/kategori_bloc.dart';

enum AnswerDialog { ok, batal }

class addTask extends StatefulWidget {
  @override
  _addTaskState createState() => _addTaskState();
}

class _addTaskState extends State<addTask> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController namaTask = TextEditingController();
  TextEditingController noteController = TextEditingController();
  Kategori dataKategori;

  //bloc
  AddTaskBloc _addTask;
  KategoriBloc _kategoriBloc;

  DateTime selectedDate = DateTime.now();
  final DateFormat format = DateFormat('yyyy-MM-dd');

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    String tanggal = format.format(selectedDate);
    _addTask = BlocProvider.of<AddTaskBloc>(context);
    _kategoriBloc = BlocProvider.of<KategoriBloc>(context);
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            "New Task",
            style: TextStyle(color: Colors.grey[700]),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        keyboardType: TextInputType.multiline,
                        decoration: const InputDecoration(
                          hintText: '',
                          labelText: 'What Are you planing?',
                        ),
                        maxLines: 5,
                        controller: namaTask,
                      ),
                      padding: EdgeInsets.all(20),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        _selectDate(context);
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 20, right: 20, bottom: 5, top: 5),
                        child: Row(
                          children: [
                            Icon(Icons.alarm),
                            SizedBox(
                              width: 10,
                            ),
                            Text(tanggal)
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        _editNote(context: context);
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 20, right: 20, bottom: 5, top: 5),
                        child: Row(
                          children: [
                            Icon(
                              Icons.note,
                              color: Colors.grey[400],
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              (noteController.text == '' ||
                                      noteController.text == null)
                                  ? "Add Note"
                                  : noteController.text,
                              style: TextStyle(color: Colors.grey[400]),
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        _selectKategori(context: context);
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 20, right: 20, bottom: 5, top: 5),
                        child: Row(
                          children: [
                            Icon(Icons.more, color: Colors.grey[400]),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              (dataKategori == null)
                                  ? "Category"
                                  : dataKategori.namaKategori,
                              style: TextStyle(color: Colors.grey[400]),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                if (dataKategori == null) {
                  // Toast.show(message: "Your toast message",
                  //     duration: Delay.SHORT,
                  //     textColor: Colors.black);
                } else if (_formKey.currentState.validate()) {
                  _showDialog(context: context);
                  _addTask.add(createTask(Task(
                      dataKategori.idKategori,
                      noteController.text,
                      tanggal.toString(),
                      namaTask.text,
                      1)));
                }
              },
              child: Container(
                height: 40,
                color: primaryColor,
                child: Center(
                    child: Text(
                  "Create",
                  style: TextStyle(color: Colors.white),
                )),
              ),
            )
          ],
        ));
  }

  Future<void> _showDialog({BuildContext context}) async {
    switch (await showDialog<AnswerDialog>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return BlocBuilder<AddTaskBloc, AddTaskState>(
              builder: (context, state) {
            if (state is AddTaskInitial) {
              return Dialog(
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Container(
                    color: Colors.transparent,
                    width: 50,
                    height: 50,
                    child: Center(child: CircularProgressIndicator())),
              );
            } else if (state is successPost) {
              Future.delayed(const Duration(seconds: 2),
                  () => Navigator.pop(context, AnswerDialog.ok));
              return Dialog(
                elevation: 0,
                backgroundColor: Colors.white,
                child: Container(
                    color: Colors.transparent,
                    width: 50,
                    height: 50,
                    child: Center(child: Text("Sukses Simpan Data"))),
              );
            } else {
              Future.delayed(const Duration(seconds: 2),
                  () => Navigator.pop(context, AnswerDialog.batal));
              return Dialog(
                elevation: 0,
                backgroundColor: Colors.white,
                child: Container(
                    color: Colors.transparent,
                    width: 50,
                    height: 50,
                    child: Center(child: Text("Gagal Simpan Data"))),
              );
            }
          });
        })) {
      case AnswerDialog.ok:
        namaTask.clear();
        _addTask.add(clear());
        Navigator.pop(context);
        break;
      case AnswerDialog.batal:
        break;
    }
  }

  Future<void> _editNote({BuildContext context}) async {
    final _formKeyNote = GlobalKey<FormState>();

    switch (await showDialog<AnswerDialog>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            child: Form(
              key: _formKeyNote,
              child: Container(
                constraints: BoxConstraints(
                    minHeight: 10, minWidth: 10, maxHeight: 500, maxWidth: 300),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        maxLength: 50,
                        controller: noteController,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Wajib di isi';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          labelText: "Note",
                        ),
                        onChanged: (value) {},
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    CupertinoButton(
                      padding: EdgeInsets.zero,
                      child: Text(
                        "save",
                        style: TextStyle(color: primaryColor),
                      ),
                      onPressed: () {
                        setState(() {});
                        Navigator.pop(context, AnswerDialog.ok);
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        })) {
      case AnswerDialog.ok:
        print(noteController.text);
        // noteController.clear();
        break;
      case AnswerDialog.batal:
        break;
    }
  }

  Future<void> _selectKategori({BuildContext context}) async {
    switch (await showDialog<AnswerDialog>(
        context: context,
        // barrierDismissible: false,
        builder: (BuildContext context) {
          return BlocBuilder<KategoriBloc, KategoriState>(
            builder: (context, state) {
              if (state is KategoriInitial) {
                _kategoriBloc.add(getKategori());
              }
              return Dialog(
                child: Container(
                  constraints: BoxConstraints(
                      minHeight: 10,
                      minWidth: 10,
                      maxHeight: 300,
                      maxWidth: 300),
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      height: 1,
                      thickness: 1,
                      indent: 10,
                      endIndent: 10,
                    ),
                    shrinkWrap: true,
                    itemCount: state.listKategori.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          dataKategori = state.listKategori[index];
                          setState(() {});
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text(state.listKategori[index].namaKategori),
                        ),
                      );
                    },
                  ),
                ),
              );
            },
          );
        })) {
      case AnswerDialog.ok:
        print(noteController.text);
        // noteController.clear();
        break;
      case AnswerDialog.batal:
        break;
    }
  }
}
