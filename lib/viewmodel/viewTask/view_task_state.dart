part of 'view_task_bloc.dart';

@immutable
abstract class ViewTaskState {
  List<Task> lateTask = List<Task>();
  List<Task> todayTask = List<Task>();
  List<Task> doneTask = List<Task>();
}

class ViewTaskInitial extends ViewTaskState {}

class setTaskState extends ViewTaskState {
  List<Task> lateTask = List<Task>();
  List<Task> todayTask = List<Task>();
  List<Task> doneTask = List<Task>();

  setTaskState({this.doneTask, this.lateTask, this.todayTask});
}
