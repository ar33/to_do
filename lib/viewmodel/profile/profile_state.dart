part of 'profile_bloc.dart';

@immutable
abstract class ProfileState {
  User dataUser;
}

class ProfileInitial extends ProfileState {}

class setProfileState extends ProfileState {
  User dataUser;

  setProfileState(this.dataUser);
}
