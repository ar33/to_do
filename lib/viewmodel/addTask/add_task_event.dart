part of 'add_task_bloc.dart';

@immutable
abstract class AddTaskEvent {
  Task dataTask;
}

class createTask extends AddTaskEvent {
  Task dataTask;

  createTask(this.dataTask);
}

class updateTask extends AddTaskEvent {
  Task dataTask;

  updateTask(this.dataTask);
}

class clear extends AddTaskEvent {
  clear();
}
